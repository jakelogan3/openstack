heat_template_version: 2015-10-15

description: T10 Student Base Environment

parameters:

  student_id:
    type: string
    label: Student ID
    description: Student ID Number
    constraints:
      -  allowed_pattern: "[1][0-9][0-9]"
         description: Student ID must be between 100 and 199

  last_name:
    type: string
    label: Last Name
    description: Last Name
    default: 
    constraints:
      -  length: { min: 1, max: 15 }
         description: Last name must be no longer than 15 characters
      -  allowed_pattern: "[a-zA-Z]*"
         description: Last name may only contain letters

  root_password:
    type: string
    label: Root Password
    description: root password for *nix VMs
    hidden: true
    default: changeme
    constraints:
      -  length: { min: 8, max: 20 }
         description: Password must be between 8 and 20 characters
      -  allowed_pattern: "[a-zA-Z0-9]*"
         description: Password may not contain special characters

resources:

  base-network:
    type: OS::Neutron::Net
    properties:
      name:
        str_replace:
          template: lastName_Network
          params:
            lastName: { get_param: last_name }

  base-subnet:
    type: OS::Neutron::Subnet
    properties:
      allocation_pools:
        - start:
            str_replace:
              template: 10.studentID.0.5
              params:
                studentID: { get_param: student_id }
          end:
            str_replace:
              template: 10.studentID.0.250
              params:
                studentID: { get_param: student_id }
      cidr:
        str_replace:
          template: 10.studentID.0.0/24
          params:
            studentID: { get_param: student_id }
      gateway_ip:
        str_replace:
          template: 10.studentID.0.254
          params:
            studentID: { get_param: student_id }
      network: { get_resource: base-network }
      dns_nameservers:
        str_split: 
          - ','
          - str_replace:
              template: 10.studentID.0.1,172.16.0.254
              params:
                studentID: { get_param: student_id }            
      name:
        str_replace:
          template: lastname_subnet
          params:
            lastname: { get_param: last_name }

  base-router:
    type: OS::Neutron::Router    
    properties:
      name:
        str_replace:
          template: lastname_router
          params:
            lastname: { get_param: last_name }
      external_gateway_info: {"network": Floating Network}

  base-router-interface:
    type:  OS::Neutron::RouterInterface
    properties:
      router_id: { get_resource: base-router }       
      subnet_id: { get_resource: base-subnet }

  server0:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-base
          params:
            lastname: { get_param: last_name }
      image: Debian Jessie
      flavor: m1.medium
      networks: 
        - network: { get_resource: base-network }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            echo "root:$password" | chpasswd
            sed -i 's/localhost/localhost baseHostname/g' /etc/hosts
            sed -i 's/#PermitRootLogin without-password/PermitRootLogin yes/g' /etc/ssh/sshd_config
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.1.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.2.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.3.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.4.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 172.16.0.0/24 -j LOG --log-level 2
            echo "kern.crit              @172.16.0.26:514" >> /etc/rsyslog.conf
            mkdir /etc/iptables
            iptables-save > /etc/iptables/rules.v4
            iptables-restore < /etc/iptables/rules.v4
            sed -i 's#exit 0#iptables-restore < /etc/iptables/rules.v4#g' /etc/rc.local
            service rsyslog restart
            apt-get update
            apt-get -y install curl
            curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > /tmp/msfinstall
            chmod +x /tmp/msfinstall
            /tmp/msfinstall
            rm /tmp/msfinstall
            DEBIAN_FRONTEND=noninteractive apt-get -y install lxde
            apt-get -y install freerdp
            apt-get -y install git
            git clone https://github.com/LionSec/katoolin.git && cp katoolin/katoolin.py /usr/bin/katoolin
            chmod +x /usr/bin/katoolin
            git clone https://github.com/n1nj4sec/pupy.git pupy
            git submodule init
            git submodule update --depth 1
            easy_install pip
            pip install -r ./pupy/requirements.txt
            git clone https://github.com/adaptivethreat/EmPyre.git empyre
            curl -L https://bitbucket.org/jakelogan3/openstack/downloads/em_survey_shell.rb > /root/Desktop/survey_shell.rb
            reboot
          params:
            $password: { get_param: root_password }
            studentID: { get_param: student_id }
            baseHostname: 
              str_replace:
                template: lastname-base
                params: 
                  lastname: { get_param: last_name }      
      user_data_format: RAW

  server1:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-centos
          params:
            lastname: { get_param: last_name }
      image: CentOS 7
      flavor: m1.small
      networks: 
        - network: { get_resource: base-network }
      user_data: 
        str_replace:
          template: |
            #!/bin/bash
            echo "root:$password" | chpasswd
            sed -i 's/localhost/localhost baseHostname/g' /etc/hosts
            sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
            sed -i 's/#PermitRootLogin yes/PermitRootLogin yes/g' /etc/ssh/sshd_config
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.1.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.2.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.3.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 10.studentID.4.0/24 -j LOG --log-level 2
            iptables -I OUTPUT -m state --state NEW -p tcp -d 172.16.0.0/24 -j LOG --log-level 2
            echo "kern.crit              @172.16.0.26:514" >> /etc/rsyslog.conf
            echo "[Install]" >> /lib/systemd/system/rc-local.service
            echo "WantedBy=multi-user.target" >> /lib/systemd/system/rc-local.service
            chmod 777 /etc/rc.d/rc.local
            systemctl enable rc-local.service
            systemctl start rc-local.service
            mkdir /etc/iptables
            iptables-save > /etc/iptables/rules.v4
            iptables-restore < /etc/iptables/rules.v4
            sed -i 's#touch /var/lock/subsys/local#iptables-restore < /etc/iptables/rules.v4#g' /etc/rc.d/rc.local
            echo "touch /var/lock/subsys/local" >> /etc/rc.d/rc.local
            reboot
          params:
            $password: { get_param: root_password }
            studentID: { get_param: student_id }
            baseHostname: 
              str_replace:
                template: lastname-centos
                params: 
                  lastname: { get_param: last_name }      
      user_data_format: RAW

  server2:
    type: OS::Nova::Server
    properties:
      name:
        str_replace:
          template: lastname-win7
          params:
            lastname: { get_param: last_name }
      image: Windows 7
      flavor: m1.medium
      networks: 
        - network: { get_resource: base-network }
      user_data:
        str_replace:
          template: |
            #ps1_sysnative
            $ErrorActionPreference = 'Stop'
            netsh advfirewall set allprofiles state off
            set-ItemProperty -Path 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Policies\System' -name "dontdisplaylastusername" -Value 1
            set-ItemProperty -Path 'HKLM:\System\CurrentControlSet\Control\Terminal Server'-name "fDenyTSConnections" -Value 0
            net user /add lastname password /y
            net localgroup administrators /add lastname
            (new-object System.Net.WebClient).DownloadFile('http://172.16.0.26/filedrop/win/base_win7.ps1','C:\init.ps1')
            iex "powershell.exe -executionpolicy bypass -file C:\init.ps1"
            del C:\init.ps1
            exit 1001
          params:
            password: { get_param: root_password }
            lastname: { get_param: last_name }
